var app = angular.module('chartApp', []);

app.controller('chartController', function($scope) {
	$scope.recipes = [];
	$scope.numOfTrays = "0";
	$scope.selectedRecipeIndex = 0;

	$scope.colorCodes = [
		'#1F77B4',
		'#AEC7E8',
		'#FF7F0E',
		'#3C763D'
	];

	$scope.loadRecipes = function() {
		$.get("recipe-data", function(data, status) {
			var data = JSON.parse(data);
			var requiredData = [];

			for (var index in data.rows) {
				requiredData.push({
					createdOn: data.rows[index].doc.createdOn,
					loadingTemperature: data.rows[index].doc.implementation.loadingTemperature,
					targetTemperature: data.rows[index].doc.implementation.targetTemperature,
					name: data.rows[index].doc.name,
					id: data.rows[index].id,
					duration: data.rows[index].doc.implementation.duration,
					ids: []
				});
			}

			var sortedData = _.orderBy(requiredData, ['name', 'createdOn'], ['asc', 'desc']);

			var uniqueRows = [sortedData[0]];
			var index = 0;

			for (var i = 1; i < sortedData.length; i++) {
				if (uniqueRows[index].name !== sortedData[i].name) {
					uniqueRows[index].ids.push(uniqueRows[index].id);
					index++;
					uniqueRows.push(sortedData[i]);
				} else {
					uniqueRows[index].ids.push(sortedData[i].id);
				}
			}

			var finalData = [];

			for (var index in uniqueRows) {
				var record = {
					name: uniqueRows[index].name,
					values: uniqueRows[index].ids,
					points: [{
						x: 0,
						y: uniqueRows[index].loadingTemperature
					}]
				};
				var i = 0;

				for (i in uniqueRows[index].targetTemperature) {
					record.points.push({
						x: uniqueRows[index].targetTemperature[i].start * 60,
						y: uniqueRows[index].targetTemperature[i].temperature
					})
				}

				record.points.push({
					x: uniqueRows[index].duration,
					y: uniqueRows[index].targetTemperature[i].temperature
				})

				finalData.push(record);
			}


			for (var index in finalData) {
				var ind = 0;

				while (ind < finalData[index].points.length - 1) {

					if (finalData[index].points[ind].y != finalData[index].points[ind + 1].y) {
						finalData[index].points.splice(ind + 1, 0, {
							x: finalData[index].points[ind + 1].x,
							y: finalData[index].points[ind].y
						});

						ind++;
					}

					ind++;
				}

			}

			finalData = _.filter(finalData, function(o) {
				return o.name !== 'EasyClean';
			});

			$scope.recipes = finalData;
			$scope.$apply();
			// $scope.changeRecipe();

			$('#recipes option')
				.filter(function() {
					return $.trim(this.text).length == 0;
				})
				.remove();
		});
		//
		// $.get("recipes", function(data, status) {
		// 	var data = JSON.parse(data).rows;
		// 	data = _.sortBy(data, ['value']);
		//
		// 	var requiredData = [];
		//
		// 	requiredData.push({
		// 		'name': data[0].value,
		// 		'values': [data[0].id]
		// 	});
		//
		// 	var slicedData = data.slice(1);
		//
		// 	for (var key in slicedData) {
		//
		// 		if (requiredData[requiredData.length - 1].name === slicedData[key].value) {
		// 			requiredData[requiredData.length - 1]['values'].push(slicedData[key].id);
		// 		} else {
		// 			requiredData.push({
		// 				'name': slicedData[key].value,
		// 				'values': [slicedData[key].id]
		// 			});
		// 		}
		// 	}
		//
		// 	$scope.recipes = requiredData;
		// 	$scope.$apply();
		// 	$scope.changeRecipe();
		//
		// 	$('#recipes option')
		// 		.filter(function() {
		// 			return $.trim(this.text).length == 0;
		// 		})
		// 		.remove();
		//
		// });
	};

	$scope.changeRecipe = function() {
		var request = {};
		$scope.responseData = [];
		$("body").isLoading({
			text: "Applying Filters",
			position: "inside",
			'tpl': '<span class="isloading-wrapper" style="top: 239.5px;">Loading <i class="glyphicon glyphicon-refresh icon-spin"></i></span>'
		});

		var trays = $scope.numOfTrays.toString().split(",");
		var flagFirstEntry = true;
		var requests = [];

		for (var counter in trays) {
			for (var index in $scope.recipes[$scope.selectedRecipeIndex].values) {
				requests.push($.post("recipe-temp-comp/", {
					ids: $scope.recipes[$scope.selectedRecipeIndex].values[index],
					startTime: ($scope.startDate ? moment($scope.startDate).utc().valueOf() : $scope.startDate),
					endTime: ($scope.endDate ? moment($scope.endDate).utc().valueOf() : $scope.endDate),
					trays: parseInt(trays[counter])
				}).then(
					function(data) {
						var d = JSON.parse(data);

						if (data != null && d.rows.length) {
							$scope.responseData.push(d);
							$scope.$apply();
						}
					}
				));
			}
		}

		$.when.apply($, requests).then(
			function(data) {
				var data = {
					rows: []
				};

				for (var index in $scope.responseData) {
					if ($scope.responseData[index].rows) {
						data.rows.push($scope.responseData[index].rows[0]);
					}
				}

				var requiredData = [];
				var finalData = [];

				$scope.pieData = $scope.initializePieData(0, 0, 0);

				for (var i = 0; i < data.rows.length; i++) {
					$scope.setUpPieData(data.rows[i].value.ovenLoad);
				}

				if (data.rows.length > 1) {
					data.rows.sort(function(obj1, obj2) {
						return Object.keys(obj1.value.tempcurve).length < Object.keys(obj2.value.tempcurve).length;
					});

					var targetObject = data.rows[0];

					for (var time in targetObject.value.tempcurve) {
						var mergedData = targetObject.value.tempcurve[time].data;

						for (var i = 1; i < data.rows.length; i++) {
							if (data.rows[i].value.tempcurve[time]) {
								mergedData = _.concat(mergedData, data.rows[i].value.tempcurve[time].data);
							}
						}

						var min = Math.min.apply(null, mergedData);
						var max = Math.max.apply(null, mergedData);

						finalData.push({
							time: time,
							min: min,
							max: max,
							mean: ((min + max) / 2)
						});
					}

				} else if (data.rows.length > 0) {
					for (var time in data.rows[0].value.tempcurve) {
						finalData.push({
							time: time,
							min: data.rows[0].value.tempcurve[time].min,
							max: data.rows[0].value.tempcurve[time].max,
							mean: ((data.rows[0].value.tempcurve[time].min + data.rows[0].value.tempcurve[time].max) / 2)
						});
					}
				}

				var minArr = [],
					maxArr = [],
					meanArr = []

				for (var index in finalData) {
					minArr.push({
						x: parseInt(finalData[index].time),
						y: parseInt(finalData[index].min)
					});
					maxArr.push({
						x: parseInt(finalData[index].time),
						y: parseInt(finalData[index].max)
					});
					meanArr.push({
						x: parseInt(finalData[index].time),
						y: parseInt(finalData[index].mean)
					});
				}

				requiredData.push({
					min: minArr,
					max: maxArr,
					mean: meanArr
				});

				$scope.setUpGraph(requiredData);
				// }
			});


		//
		// request.then(function(data, status) {
		// 	var data = JSON.parse(data);
		// 	var requiredData = [];
		// 	var finalData = [];
		//
		// 	if (data.rows.length > 1) {
		// 		data.rows.sort(function(obj1, obj2) {
		// 			return Object.keys(obj1.value.tempcurve).length < Object.keys(obj2.value.tempcurve).length;
		// 		});
		//
		// 		var targetObject = data.rows[0];
		//
		// 		for (var time in targetObject.value.tempcurve) {
		// 			var mergedData = targetObject.value.tempcurve[time].data;
		//
		// 			for (var i = 1; i < data.rows.length; i++) {
		// 				if (data.rows[i].value.tempcurve[time]) {
		// 					mergedData = _.concat(mergedData, data.rows[i].value.tempcurve[time].data);
		// 				}
		// 			}
		//
		// 			var min = Math.min.apply(null, mergedData);
		// 			var max = Math.max.apply(null, mergedData);
		//
		// 			finalData.push({
		// 				time: time,
		// 				min: min,
		// 				max: max,
		// 				mean: ((min + max) / 2)
		// 			});
		// 		}
		//
		// 	} else if (data.rows.length > 0) {
		// 		for (var time in data.rows[0].value.tempcurve) {
		// 			finalData.push({
		// 				time: time,
		// 				min: data.rows[0].value.tempcurve[time].min,
		// 				max: data.rows[0].value.tempcurve[time].max,
		// 				mean: ((data.rows[0].value.tempcurve[time].min + data.rows[0].value.tempcurve[time].max) / 2)
		// 			});
		// 		}
		// 	}
		//
		// 	var minArr = [],
		// 		maxArr = [],
		// 		meanArr = []
		//
		// 	for (var index in finalData) {
		// 		minArr.push({
		// 			x: parseInt(finalData[index].time),
		// 			y: parseInt(finalData[index].min)
		// 		});
		// 		maxArr.push({
		// 			x: parseInt(finalData[index].time),
		// 			y: parseInt(finalData[index].max)
		// 		});
		// 		meanArr.push({
		// 			x: parseInt(finalData[index].time),
		// 			y: parseInt(finalData[index].mean)
		// 		});
		// 	}
		//
		// 	requiredData.push({
		// 		min: minArr,
		// 		max: maxArr,
		// 		mean: meanArr
		// 	});
		//
		// 	$scope.setUpGraph(requiredData);
		// });

	};

	$scope.setUpPieData = function(record) {
		$scope.pieData = $scope.initializePieData(
			parseInt($scope.pieData[0].value) + parseInt(record[1]),
			parseInt($scope.pieData[1].value) + parseInt(record[2]),
			parseInt($scope.pieData[2].value) + parseInt(record[3])
		);
	};

	$scope.initializePieData = function(value1, value2, value3) {
		return [{
			label: '1',
			value: value1
		}, {
			label: '2',
			value: value2
		}, {
			label: '3',
			value: value3
		}, ]
	};

	$scope.setUpGraph = function(data) {

		nv.addGraph(function() {
			var chart = nv.models.pieChart()
				.x(function(d) {
					return d.label
				})
				.y(function(d) {
					return d.value
				})
				.showLabels(true);

			d3.select("#pieChart svg")
				.datum($scope.pieData)
				.transition().duration(350)
				.call(chart);

			return chart;
		});

		nv.addGraph(function() {
			var chart = nv.models.lineChart()
				.margin({
					left: 100
				});

			// chart.useInteractiveGuideline(true);
			// chart.lines({
			// 	interactive: true
			// });
			var tickValues = [0, 25, 50, 75, 100, 125, 150, 175, 200, 225, 250];
			chart.forceY(tickValues);

			chart.yAxis.tickValues(tickValues)
				.tickFormat(function(d) {
					return tickformat[d];
				});

			chart.showLegend(true);
			chart.showYAxis(true);
			// chart.interpolate("step-after");
			chart.showXAxis(true);

			var formatTime = d3.time.format("%M"),
				formatMinutes = function(d) {
					return formatTime(new Date(2012, 0, 1, 0, 0, d));
				};

			chart.xAxis
				.axisLabel('Time (minutes)')
				.tickFormat(formatMinutes);

			chart.yAxis
				.axisLabel('Temperature (°C)')
				.tickFormat(d3.format('.02f'));

			var myData = function(data) {
				var chartData = [];

				for (var key in data[0]) {
					chartData.push({
						values: data[0][key],
						key: key
					});
				}

				return chartData;
			}(data);

			var expectedLine = {};
			expectedLine.key = 'Expected Temperature';
			expectedLine.values = $scope.recipes[$scope.selectedRecipeIndex].points;
			myData.push(expectedLine);

			for (var counter in myData) {
				myData[counter].color = $scope.colorCodes[counter];
			}


			var xValues = myData.map(d => {
				return d.values.map(x => x.x);
			});

			xValues = _.concat.apply(this, xValues);

			var stepSize = Math.ceil(Math.max.apply(this, xValues) / 600);
			var steps = _.range(0, stepSize * 10, stepSize);


			var xValues = myData.map(d => {
				return d.values.map(x => x.x);
			})

			xValues = _.concat.apply(this, xValues);

			var stepSize = Math.max.apply(this, xValues) / 10;
			var steps = _.range(0, stepSize * 10, stepSize);

			console.log(steps);
			//

			chart.xAxis
				.axisLabel('Time (minutes)')
				.tickValues(steps);

			d3.select('#lineChart svg')
				.datum(myData)
				.call(chart);

			nv.utils.windowResize(function() {
				chart.update()
			});

			return chart;
		});

		$("body").isLoading("hide");
	}

	$scope.loadRecipes();
});
