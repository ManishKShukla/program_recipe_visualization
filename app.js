var request = require('request');
var express = require("express");
var app = express();
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({
	extended: false
}));
app.use(bodyParser.json());

var router = express.Router();
var path = require('path');
var _path = __dirname + '/views/';
var config = require('./config/config')

router.use(function(req, res, next) {
	console.log("/" + req.method);
	next();
});

router.get("/recipe-temp", function(req, res) {
	res.sendFile(_path + "recipe-temp-comp.html");
});

router.get("/", function(req, res) {
	res.sendFile(_path + "index.html");
});

router.get("/recipe-data", function(req, res) {
	console.log(config.api_url);
	request(config.api_url, function(error, response, body) {
		if (!error && response.statusCode == 200) {
			res.send(body);
		} else {
			res.send(JSON.stringify(error));
		}
	});
});

router.get("/recipes", function(req, res) {
	request(config.recipes, function(error, response, body) {
		if (!error && response.statusCode == 200) {
			res.send(body);
		} else {
			res.send(JSON.stringify(error));
		}
	});
});

router.post("/recipe-temp-comp", function(req, res) {
	var param = req.params.id;

	var url = config.recipe_temp_comp_url + "startkey=[\"" + req.body.ids + "\"," + req.body.trays + ",";
	url = url + (req.body.startTime ? req.body.startTime : null) + "]&"

	url = url + "endkey=[\"" + req.body.ids + "\"," + req.body.trays + ",";
	url = url + (req.body.endTime ? req.body.endTime : "{}") + "]"

	url = url + "&reduce=true&include_docs=false&group=true&group_level=1";

	console.log(url);

	request(url, function(error, response, body) {
		if (!error && response.statusCode == 200) {
			res.send(body);
		} else {
			res.send(JSON.stringify(error));
		}
	});
});

router.get("/contact", function(req, res) {
	res.sendFile(path + "contact.html");
});

app.use("/", router);

app.use('/img', express.static(path.join(__dirname, 'libs/images')));
app.use('/js', express.static(path.join(__dirname, 'libs/js')));
app.use('/css', express.static(path.join(__dirname, 'libs/css')));

app.listen(config.port, function() {
	console.log("Live at Port " + config.port);
});
